# Ruby on Rails application builder images

These images are based on the Docker official Ruby images.
`GEM_HOME` is set to `/usr/local/bundle` and Bundler is
configured to install gems to that location. (To further
ensure all gems are installed to `GEM_HOME`, `BUNDLE_USER_HOME`
is also set to `GEM_HOME`.)

Supported Ruby major versions: 2.6, 2.7, and 3.0.  Minor version
is the latest within the major version.

Rails versions have been pinned to the Ruby major version:

	Ruby 2.6: Rails 5.2
	Ruby 2.7: Rails 6.0
	Ruby 3.0: Rails 6.1

*This image only supports PostgreSQL as the Rails database.*

## Usage

The simplest usage in a Rails project is as follows:

	$ cd my-app
	$ echo "FROM gitlab-registry.oit.duke.edu/devops/ruby-base:2.6" > ./Dockerfile
	$ docker build -t my-app --pull .

## Build Options

Onbuild arguments:

	EXTRA_DEPENDENCIES  Additional Debian packages to install
	NODEJS_MAJOR[=12]   Major version of Node.js to install via Nodesource
	RAILS_PORT[=3000]   TCP port the Rails server will listen on

## Runtime Options

By default the assembled runtime image runs `rails server`.  You can override the
command in the usual Docker fashion.  If you also override the entrypoint, you
should ensure that the container runs as a non-root user.  (The default entrypoint
uses `gosu` to step down from root.) The base image provides
the user `app-user` (UID 1001, GID 0) for this purpose.  If you use a different
user you may need to ensure the user a member of group `root` (GID 0).

In Openshift the container will run as a random UID with GID 0.

Runtime variables:

	RAILS_ENV[=production]       Rails environment
	RAILS_LOG_LEVEL[=info]       Log level of Rails logger (STDOUT)
	RAILS_MAX_THREADS[=5]        Number of threads per worker of Rails server
	RAILS_SERVE_STATIC_FILES[=1] Whether to serve static files with the Rails server
    STARTUP_RAKE_TASKS           A list of rake tasks to run before starting the server (or other command)
	WEB_CONCURRENCY[=2]          Number of workers for the Rails server

## Building this builder image

    $ make [ruby_major=[2.6|2.7|3.0]]

Build options:

	ruby_major[=2.6] -- Ruby major version of base image

## Test

	$ make clean test [ruby_major=[2.6|2.7|3.0]]
