##
## Production puma config
##

#
# Threads
#
# Note: Puma >= 5 recognizes env vars:
#
# - PUMA_MIN_THREADS
# - PUMA_MAX_THREADS
# - MIN_THREADS
# - MAX_THREADS
#
threads_count = ENV.fetch("RAILS_MAX_THREADS", 5)
threads threads_count, threads_count

workers ENV.fetch("WEB_CONCURRENCY", 2)
preload_app!

port ENV.fetch("RAILS_PORT", "3000")
