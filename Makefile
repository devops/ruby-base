SHELL = /bin/bash

ruby_major ?= 2.6
build_tag ?= rails-base:ruby-$(ruby_major)
test_tag = $(build_tag)-test

ifdef CI_COMMIT_SHORT_SHA
	test_container = app-ruby-$(ruby_major)-$(CI_COMMIT_SHORT_SHA)
else
	test_container = app-ruby-$(ruby_major)-test
endif

ifeq ($(ruby_major), 2.6)
	rails_minor = 5.2
else ifeq ($(ruby_major), 2.7)
	rails_minor = 6.0
else ifeq ($(ruby_major), 3.0)
	rails_minor = 6.1
endif

rails_test_app = gitlab-registry.oit.duke.edu/devops/containers/rails-test-app:rails-$(rails_minor)-ruby-$(ruby_major)-main

.PHONY : build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) \
		--build-arg ruby_major=$(ruby_major) \
		./src

.PHONY : clean
clean:
	rm -rf ./test-app

.PHONY : test
test: test-image
	test_tag=$(test_tag) test_container=$(test_container) ./test.sh

.PHONY : test-image
test-image: test-app
	echo "FROM $(build_tag)" > ./test-app/Dockerfile
	DOCKER_BUILDKIT=1 docker build -t $(test_tag) ./test-app

test-app:
	docker pull $(rails_test_app)
	docker run --rm -d --name rails-test-app $(rails_test_app) /bin/bash -c 'sleep infinity'
	docker cp rails-test-app:/usr/src/app ./test-app
	docker stop rails-test-app
